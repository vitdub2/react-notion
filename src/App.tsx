import SignIn from './pages/SignIn'

const App = (): JSX.Element => {
  return <SignIn />
}

export default App
