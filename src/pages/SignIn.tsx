import { Box, Button, Container, TextField, Typography } from '@mui/material'
import styled from '@emotion/styled'

import { Google as GoogleIcon } from '../icons/Google'

const Main = styled.main`
  display: flex;
  align-items: center;
  flex-grow: 1;
  min-height: 100%;
`

const SignIn = (): JSX.Element => {
  return (
    <Main>
      <Container maxWidth="sm">
        <form>
          <Box sx={{ my: 3 }}>
            <Typography variant="h4">Sign in</Typography>
          </Box>
          <Box>
            <Button
              startIcon={<GoogleIcon />}
              fullWidth
              color="error"
              size="large"
              variant="contained"
            >
              Login with Google
            </Button>
          </Box>
          <Box sx={{ pb: 1, pt: 3 }}>
            <Typography align="center" color="textSecondary" variant="body1">
              or login with email address
            </Typography>
          </Box>
          <TextField
            fullWidth
            label="Email Address"
            margin="normal"
            name="email"
            type="email"
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Password"
            margin="normal"
            name="password"
            type="password"
            variant="outlined"
          />
          <Box sx={{ py: 2 }}>
            <Button
              color="primary"
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              Sign In Now
            </Button>
          </Box>
          <Typography color="textSecondary" variant="body2">
            Don&apos;t have an account? <a href="/register">Sign Up</a>
          </Typography>
        </form>
      </Container>
    </Main>
  )
}

export default SignIn
